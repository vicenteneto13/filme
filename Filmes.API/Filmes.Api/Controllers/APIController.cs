﻿using Filmes.Api.Domain.Exceptions;
using Filmes.Api.Results;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace Filmes.Api.Controllers
{
    public class APIController : Controller
    {

        protected string EmailUsuario
        {
            get => User.Claims?
                    .Where(c => c.Type == ClaimTypes.Email)?
                    .Select(c => c.Value)?
                    .FirstOrDefault();
        }

        protected GenericResult Executar(Func<dynamic> action)
        {
            ValidarModel();

            return new GenericResult()
            {
                Code = (int)HttpStatusCode.OK,
                Success = action()
            };
        }

        protected GenericResult<T> Executar<T>(Func<dynamic> action)
        {
            ValidarModel();

            return new GenericResult<T>()
            {
                Code = (int)HttpStatusCode.OK,
                Result = action(),
                Success = true
            };
        }

        private void ValidarModel()
        {
            if (!ModelState.IsValid)
                throw new InvalidModelException("Model / DTO inválido");
        }
    }
}
