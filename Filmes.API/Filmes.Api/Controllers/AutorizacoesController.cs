﻿using Filmes.Api.CrossCutting.Token;
using Filmes.Api.Domain.Interface.Services.Acesso;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace Filmes.Api.Controllers
{
    [Route("api/[controller]")]
    public class AutorizacoesController : APIController
    {
        private readonly IUsuarioService _service;

        public AutorizacoesController(IUsuarioService usuarioService)
        {
            _service = usuarioService;
        }

        /// <summary>
        /// Realiza autenticação na API. Obtêm o token de acesso.
        /// </summary>
        /// <param name="email">E-mail do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("{email}/{senha}")]
        public object Post(string email, string senha,
            [FromServices] SigningConfigurations signingConfigurations,
            [FromServices] TokenConfigurations tokenConfigurations)
        {

            var usuario = _service.ValidarAcesso(email, senha);

            if (usuario.Valido)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(email, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, usuario.Id.ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, usuario.Email),
                        new Claim(JwtRegisteredClaimNames.GivenName, usuario.Nome),
                    }
                );

                identity.AddClaim(new Claim(ClaimTypes.Role, usuario.Administrador ? "Administrador" : "Usuario"));

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao + TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new
                {
                    authenticated = true,
                    created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    accessToken = token,
                    message = "OK"
                };
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                };
            }
        }
    }
}
