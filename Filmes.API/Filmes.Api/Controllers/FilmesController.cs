﻿using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Interface.Services.Midia;
using Filmes.Api.Domain.Paged;
using Filmes.Api.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Filmes.Api.Controllers
{
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class FilmesController : APIController
    {
        private readonly IMidiaService _service;

        public FilmesController(IMidiaService midiaService)
        {
            _service = midiaService;
        }

        /// <summary>
        /// Listagem paginada, podendo filtrar por diretor, nome, gênero e/ou atores.
        /// </summary>
        /// <param name="filtrosFilmeDto">Modelo dos filtros</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, Usuario")]
        [HttpGet]
        public GenericResult<PagedResult<PesquisaFilmeDto>> Get([FromQuery] FiltrosFilmeDto filtrosFilmeDto) => 
            Executar<PagedResult<PesquisaFilmeDto>>(() => _service.ListarPorFiltroAvancadoPaginado(filtrosFilmeDto));

        /// <summary>
        /// Obtém um filme determinado pelo identificador.
        /// </summary>
        /// <param name="id">Identificador do filme</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, Usuario")]
        [HttpGet("{id}")]
        public GenericResult<FilmeDto> Get([FromRoute] int id) =>
            Executar<FilmeDto>(() => _service.GetById(id));

        /// <summary>
        /// Insere dados de um novo filme. 
        /// </summary>
        /// <param name="filmeDto">Modelo a inserir</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public GenericResult Post([FromBody] NovoFilmeDto filmeDto) =>
            Executar(() => _service.Add(filmeDto));


        /// <summary>
        /// Insere dados de um novo filme. 
        /// </summary>
        /// <param name="filmeDto">Modelo a inserir</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpPut("{id}")]
        public GenericResult Put([FromRoute] int id, [FromBody] FilmeDto filmeDto) =>
            Executar(() => _service.Update(filmeDto));

        /// <summary>
        /// Remove um filme pelo identificador.
        /// </summary>
        /// <param name="id">Modelo a inserir</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpDelete("{id}")]
        public GenericResult Remove([FromRoute] int id) =>
            Executar(() => _service.Remove(id));
    }
}
