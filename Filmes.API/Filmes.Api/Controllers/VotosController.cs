﻿using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Interface.Services.Midia;
using Filmes.Api.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Filmes.Api.Controllers
{
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class VotosController : APIController
    {
        private readonly IMidiaService _service;

        public VotosController(IMidiaService midiaService)
        {
            _service = midiaService;
        }

        /// <summary>
        /// Insere o voto de 0-4 para um filme. 
        /// </summary>
        /// <param name="votoDto">Modelo a inserir</param>
        /// <returns></returns>
        [Authorize(Roles = "Usuario")]
        [HttpPost("votar")]
        public GenericResult Post([FromBody] VotarFilmeDto votoDto) =>
            Executar(() => _service.Votar(EmailUsuario, votoDto));
    }
}
