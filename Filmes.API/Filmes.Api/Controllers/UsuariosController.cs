﻿using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Interface.Services.Acesso;
using Filmes.Api.Domain.Paged;
using Filmes.Api.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Filmes.Api.Controllers
{
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class UsuariosController : APIController
    {
        private readonly IUsuarioService _service;

        public UsuariosController(IUsuarioService usuarioService)
        {
            _service = usuarioService;
        }

        /// <summary>
        /// Listagem de usuários não administradores ativos.
        /// </summary>
        /// <param name="pagina">Número da página</param>S
        /// <param name="tamanhoPagina">Número de registros por página</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, Usuario")]
        [HttpGet("{pagina}/{tamanhoPagina}")]
        public GenericResult<PagedResult<UsuarioDto>> Get(int pagina, int tamanhoPagina) =>
            Executar<PagedResult<UsuarioDto>>(() =>_service.ListarUsuariosNaoAdministradoresPaginado(pagina, tamanhoPagina));

        /// <summary>
        /// Obtém um usuário determinado pelo identificador.
        /// </summary>
        /// <param name="id">Identificador do usuário</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, Usuario")]
        [HttpGet("{id}")]
        public GenericResult<UsuarioDto> Get(int id) =>
            Executar<UsuarioDto>(() => _service.GetById(id));

        /// <summary>
        /// Insere dados de um novo usuário. 
        /// </summary>
        /// <param name="usuarioDto">Modelo a inserir</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public GenericResult Post([FromBody] NovoUsuarioDto usuarioDto) =>
            Executar(() => _service.Add(usuarioDto));

        /// <summary>
        /// Atualiza dados de um usuário existente
        /// </summary>
        /// <param name="id">Identificador do usuário</param>
        /// <param name="usuarioDto">Modelo a atualizar</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpPut("{id}")]
        public GenericResult Put([FromRoute] int id, [FromBody] UsuarioDto usuarioDto) => 
            Executar(() => _service.Update(usuarioDto));

        /// <summary>
        /// Excluir um usuário determinado pelo identificador.
        /// </summary>
        /// <param name="id">Identificador do usuário</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        [HttpDelete("{id}")]
        public GenericResult Delete([FromRoute] int id) => 
            Executar(() => _service.Desativar(id));
    }
}
