﻿namespace Filmes.Api.Results
{
    public class GenericResult
    {
        public int Code { get; set; }
        public bool Success { get; set; }
        public MessageError Error { get; set; }
    }

    public class GenericResult<TResult> : GenericResult
    {
        public TResult Result { get; set; }
    }

    public class MessageError
    {
        public string Message { get; set; }
        public string DetailMessage { get; set; }
    }
}
