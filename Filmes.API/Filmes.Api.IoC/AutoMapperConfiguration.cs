﻿using System;
using System.Collections.Generic;
using ToDoApp.AppServices.Mappings;

namespace ToDoApp.IoC
{
    public static class AutoMapperConfiguration
    {
        public static IEnumerable<Type> GetAutoMapperProfiles()
        {
            var result = new List<Type>();
            result.Add(typeof(MappingProfile));
            return result;
        }
    }
}
