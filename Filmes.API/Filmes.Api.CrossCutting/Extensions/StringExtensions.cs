﻿using System;
using System.Text;

namespace Filmes.Api.CrossCutting.Extensions
{
    public static class StringExtensions
    {
        public static string EncodeBase64(this string value) =>
            Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
        public static string DecodeBase64(this string value) => 
            Encoding.UTF8.GetString(Convert.FromBase64String(value));
    }
}
