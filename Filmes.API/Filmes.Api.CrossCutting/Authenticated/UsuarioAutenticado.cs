﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Filmes.Api.CrossCutting.Authenticated
{
    public class UsuarioAutenticado
    {
        private readonly IHttpContextAccessor _accessor;

        public UsuarioAutenticado(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Email => _accessor.HttpContext.User.Identity?.Name;
        public string Administrador => GetClaimsIdentity().FirstOrDefault(a => a.Type == "Administrador")?.Value;

        public IEnumerable<Claim> GetClaimsIdentity()
        {
            return _accessor.HttpContext.User.Claims;
        }
    }
}
