﻿using Filmes.Api.Domain.Dtos.Paginado;

namespace Filmes.Api.Domain.Dtos.Midia
{
    public class FiltrosFilmeDto : PaginaDto
    {
        public string Descricao { get; set; }
    }
}