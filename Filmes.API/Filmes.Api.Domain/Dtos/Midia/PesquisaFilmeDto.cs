﻿using System;

namespace Filmes.Api.Domain.Dtos.Midia
{
    public class PesquisaFilmeDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Diretor { get; set; }
        public DateTime Lancamento { get; set; }
        public decimal MediaVotos { get; set; }
        public int QuantidadeVotos { get; set; }
    }
}