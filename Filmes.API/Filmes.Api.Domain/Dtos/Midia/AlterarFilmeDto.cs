﻿using System;
using System.Collections.Generic;

namespace Filmes.Api.Domain.Dtos.Midia
{
    public class AlterarFilmeDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Diretor { get; set; }
        public DateTime Lancamento { get; set; }
        public IList<string> Atores { get; set; }
    }
}