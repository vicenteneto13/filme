﻿namespace Filmes.Api.Domain.Dtos.Midia
{
    public class VotarFilmeDto
    {
        public int IdFilme { get; set; }
        public int ValorVoto { get; set; }
    }
}