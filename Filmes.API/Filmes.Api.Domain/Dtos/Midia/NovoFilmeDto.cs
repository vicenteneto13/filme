﻿using System;

namespace Filmes.Api.Domain.Dtos.Midia
{
    public class NovoFilmeDto
    {
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Diretor { get; set; }
        public DateTime Lancamento { get; set; }
    }
}