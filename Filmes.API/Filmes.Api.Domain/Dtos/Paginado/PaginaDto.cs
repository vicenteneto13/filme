﻿namespace Filmes.Api.Domain.Dtos.Paginado
{
    public class PaginaDto
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}