﻿namespace Filmes.Api.Domain.Dtos.Acesso
{
    public class NovoUsuarioDto
    {
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public bool Administrador { get; set; }
    }
}
