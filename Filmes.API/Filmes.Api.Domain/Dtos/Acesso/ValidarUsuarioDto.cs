﻿namespace Filmes.Api.Domain.Dtos.Acesso
{
    public class ValidarUsuarioDto
    {
        public int Id { get; set; }
        public bool Valido { get; set; }
        public bool Administrador { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
    }
}
