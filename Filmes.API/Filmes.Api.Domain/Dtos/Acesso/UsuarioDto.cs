﻿namespace Filmes.Api.Domain.Dtos.Acesso
{
    public class UsuarioDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public bool Administrador { get; set; }
    }
}
