﻿
using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.Domain.Interface.Repositories.Acesso
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        PagedResult<UsuarioDto> ListarUsuariosNaoAdministradoresPaginado(int page, int pageSize);
        Usuario ObterUsuarioPorEmail(string email);
    }
}