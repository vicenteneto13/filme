﻿using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Entities.Midia;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.Domain.Interface.Repositories.Midia
{
    public interface IFilmeRepository : IRepositoryBase<Filme>
    {
        PagedResult<Filme> ListarPorFiltroAvancadoPaginado(FiltrosFilmeDto filtrosFilmeDto);
    }
}