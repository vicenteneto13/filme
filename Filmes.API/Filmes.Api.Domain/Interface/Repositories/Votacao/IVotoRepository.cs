﻿using Filmes.Api.Domain.Entities.Votacao;
using Filmes.Api.Domain.Paged;
using System.Collections.Generic;

namespace Filmes.Api.Domain.Interface.Repositories.Votacao
{
    public interface IVotoRepository : IRepositoryBase<Voto>
    {
        IList<Voto> ListarPorFilme(int idFilme);
    }
}