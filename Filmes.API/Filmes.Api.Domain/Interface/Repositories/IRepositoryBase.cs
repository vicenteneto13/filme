﻿using Filmes.Api.Domain.Paged;
using System.Collections.Generic;

namespace Filmes.Api.Domain.Interface.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        PagedResult<TEntity> GetAll(int page, int pageSize);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);
    }
}
