﻿using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.Domain.Interface.Services.Acesso
{
    public interface IUsuarioService : IServiceBase<Usuario, UsuarioDto>
    {
        bool Add(NovoUsuarioDto usuarioDto);
        bool Desativar(int id);
        PagedResult<UsuarioDto> ListarUsuariosNaoAdministradoresPaginado(int page, int pageSize);
        ValidarUsuarioDto ValidarAcesso(string email, string senha);
    }
}