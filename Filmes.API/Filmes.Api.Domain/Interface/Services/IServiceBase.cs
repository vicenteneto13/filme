﻿using Filmes.Api.Domain.Paged;
using System.Collections.Generic;

namespace Filmes.Api.Domain.Interface.Services
{
    public interface IServiceBase<TEntity, TEntityDto> where TEntity : class where TEntityDto : class
    {
        bool Add(TEntityDto obj);
        TEntityDto GetById(int id);
        PagedResult<TEntityDto> GetAll(int page, int pageSize);
        IEnumerable<TEntityDto> GetAll();
        bool Update(TEntityDto obj);
        bool Remove(int id);
    }
}