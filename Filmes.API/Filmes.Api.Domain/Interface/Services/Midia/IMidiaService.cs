﻿using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Entities.Midia;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.Domain.Interface.Services.Midia
{
    public interface IMidiaService : IServiceBase<Filme, FilmeDto>
    {
        bool Add(NovoFilmeDto filmeDto);
        PagedResult<PesquisaFilmeDto> ListarPorFiltroAvancadoPaginado(FiltrosFilmeDto filtrosFilmeDto);
        bool Remove(int id);
        bool Update(FilmeDto filmeDto);
        bool Votar(string emailUsuario, VotarFilmeDto votarDto);
    }
}