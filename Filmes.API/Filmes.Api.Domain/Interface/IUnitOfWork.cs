﻿namespace Filmes.Api.Domain.Interface
{
    public interface IUnitOfWork
    {
        bool Save();
    }
}
