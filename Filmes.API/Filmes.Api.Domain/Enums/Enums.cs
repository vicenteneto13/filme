﻿namespace Filmes.Api.Domain.Enums
{
    public class Enums
    {
        public enum TipoPessoas
        {
            Pessoa = 0,
            PessoaFisica = 1,
            PessoaJuridica = 2
        }

        public enum Sexo
        {
            Feminino = 0,
            Masculino = 1
        }

        public enum EstadoCivil
        {
            Solteiro = 0,
            Casado = 1,
            Divorciado = 2,
            Viuvo = 3
        }

        public enum MeioContato
        {
            TelefoneFixo = 0,
            TelefoneMovel = 1,
            TelefoneMovelWhatsapp = 2,
            Email = 3,
            Facebook = 4,
            Twitter = 5,
            Instagram = 6
        }

        public enum TipoPagamento
        {
            Dinheiro = 0,
            CartaoCredito = 1,
            CartaoDebito = 2,
            Cheque = 3,
            Duplicata = 4
        }
    }
}
