﻿using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Entities.Midia;
using Filmes.Api.Domain.Exceptions;
using System;

namespace Filmes.Api.Domain.Entities.Votacao
{
    public class Voto
    {
        public Voto()
        {
            DataCadastro = DateTime.Now;
        }

        private int _valorVoto;

        public int Id { get; set; }
        public int IdFilme { get; set; }
        public Filme Filme { get; set; }
        public int IdUsuario { get; set; }
        public Usuario Usuario { get; set; }
        public int ValorVoto
        {
            get { return _valorVoto; }
            set
            {
                if (value < 0 || value > 4)
                    throw new InvalidFieldException("Valor do voto inválido. Deve ser entre 0 e 4.");

                _valorVoto = value;
            }
        }
        public DateTime DataCadastro { get; set; }
    }
}
