﻿using Filmes.Api.Domain.Entities.Votacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Filmes.Api.Domain.Entities.Midia
{
    public class Filme
    {
        public Filme()
        {
            DataCadastro = DateTime.Now;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Diretor { get; set; }
        public DateTime Lancamento { get; set; }
        public IList<Voto> Votos { get; set; } = new List<Voto>();
        public DateTime DataCadastro { get; private set; }

        public int QuantidadeVotos => Votos.Count();
        public decimal MediaVotos => Votos.Any() ?
                                     Votos.Sum(v => v.ValorVoto) / QuantidadeVotos :
                                     0;

        public void RemoverVotos() =>
            Votos.Clear();
    }
}