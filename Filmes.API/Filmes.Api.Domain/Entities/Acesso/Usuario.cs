﻿using Filmes.Api.CrossCutting.Extensions;
using Filmes.Api.Domain.Entities.Votacao;
using Filmes.Api.Domain.Exceptions;
using System;
using System.Collections.Generic;

namespace Filmes.Api.Domain.Entities.Acesso
{
    public class Usuario
    {
        public Usuario()
        {
            DataCadastro = DateTime.Now;
            Votos = new List<Voto>();
        }

        private string _senha;

        public int Id { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha
        {
            get { return _senha; }
            set { _senha = value.EncodeBase64(); }
        }
        public bool Ativo { get; set; }
        public bool Administrador { get; set; }
        public IList<Voto> Votos { get; set; }
        public DateTime DataCadastro { get; set; }

        public bool ValidarCredencial(string senha)
        {
            if (senha.EncodeBase64() != Senha)
                throw new AuthenticaionException();
            if (!Ativo)
                throw new AuthenticaionException("Usuário sem permissão de acesso");

            return true;
        }
    }
}