﻿using System;

namespace Filmes.Api.Domain.Entities.Acesso
{
    public class Token
    {
        public int Id { get; set; }
        public DateTime Criacao { get; set; }
        public DateTime Expiracao { get; set; }
        public string TokenAcesso { get; set; }
        public Usuario Usuario { get; set; }
    }
}
