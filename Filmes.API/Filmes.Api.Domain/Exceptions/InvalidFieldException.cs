﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class InvalidFieldException : BaseCustomException
    {
        public InvalidFieldException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}