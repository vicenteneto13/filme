﻿namespace Filmes.Api.Domain.Exceptions
{
    public class CustomErrorResponse
    {
        public string Message { get; set; }
        public string Description { get; set; }
    }
}
