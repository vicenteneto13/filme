﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class NotFoundRecordException : BaseCustomException
    {
        public NotFoundRecordException(string message = "Nenhum registro encontrado.", string description = "") : 
            base(message, description, (int)HttpStatusCode.NotFound)
        {
        }
    }
}