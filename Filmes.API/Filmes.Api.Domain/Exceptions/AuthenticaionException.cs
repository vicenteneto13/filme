﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class AuthenticaionException : BaseCustomException
    {
        public AuthenticaionException(string message = "Usuário ou senha inválidos", string description = "") : 
            base(message, description, (int)HttpStatusCode.Unauthorized)
        {
        }
    }
}