﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class InvalidDomainException : BaseCustomException
    {
        public InvalidDomainException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}