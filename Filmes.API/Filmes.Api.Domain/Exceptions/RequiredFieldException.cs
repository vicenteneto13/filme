﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class RequiredFieldException : BaseCustomException
    {
        public RequiredFieldException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}