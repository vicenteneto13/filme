﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class RecordExistingException : BaseCustomException
    {
        public RecordExistingException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}