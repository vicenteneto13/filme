﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class InvalidUserPermissionException : BaseCustomException
    {
        public InvalidUserPermissionException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}