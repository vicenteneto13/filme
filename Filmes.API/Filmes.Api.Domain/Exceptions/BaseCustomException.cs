﻿using System;

namespace Filmes.Api.Domain.Exceptions
{
    public class BaseCustomException : Exception
    {
        private readonly int _code;
        private readonly string _description;

        public int Code
        {
            get => _code;
        }
        public string Description
        {
            get => _description;
        }

        public BaseCustomException(string message, string description, int code) : base(message)
        {
            _code = code;
            _description = description;
        }
    }
}
