﻿using System.Net;

namespace Filmes.Api.Domain.Exceptions
{
    public class InvalidModelException : BaseCustomException
    {
        public InvalidModelException(string message, string description = "") : 
            base(message, description, (int)HttpStatusCode.BadRequest)
        {
        }
    }
}