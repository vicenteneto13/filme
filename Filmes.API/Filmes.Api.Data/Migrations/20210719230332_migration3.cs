﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmes.Api.Data.Migrations
{
    public partial class migration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Atores",
                table: "Filme");

            migrationBuilder.AddColumn<DateTime>(
                name: "DataCadastro",
                table: "Filme",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataCadastro",
                table: "Filme");

            migrationBuilder.AddColumn<string>(
                name: "Atores",
                table: "Filme",
                type: "varchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
