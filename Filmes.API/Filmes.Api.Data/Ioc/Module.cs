﻿using Filmes.Api.Data.Repositories;
using Filmes.Api.Data.Repositories.Acesso;
using Filmes.Api.Data.Repositories.Midia;
using Filmes.Api.Data.Repositories.Votacao;
using Filmes.Api.Domain.Interface;
using Filmes.Api.Domain.Interface.Repositories;
using Filmes.Api.Domain.Interface.Repositories.Acesso;
using Filmes.Api.Domain.Interface.Repositories.Midia;
using Filmes.Api.Domain.Interface.Repositories.Votacao;
using System;
using System.Collections.Generic;

namespace Filmes.Api.Data.Ioc
{
    public static class Module
    {
        public static Dictionary<Type,Type> GetTypes()
        {
            var dic = new Dictionary<Type, Type>();
            dic.Add(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            dic.Add(typeof(IFilmeRepository), typeof(FilmeRepository));
            dic.Add(typeof(IUsuarioRepository), typeof(UsuarioRepository));
            dic.Add(typeof(IVotoRepository), typeof(VotoRepository));
            dic.Add(typeof(IUnitOfWork), typeof(UnitOfWork));

            return dic;
        }
    }
}
