﻿using Filmes.Api.Data.EntityConfig.Midia;
using Microsoft.EntityFrameworkCore;

namespace Filmes.Api.Data.Context
{
    public class FilmesApiContext : DbContext
    {
        private readonly string _dominioDados = "bdfilme";

        public FilmesApiContext()
        {
        }

        public FilmesApiContext(string dominio)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($@"Data Source=localhost;Initial Catalog={_dominioDados};Integrated Security=SSPI");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FilmeMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}