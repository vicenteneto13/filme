﻿using Filmes.Api.Data.Context;
using Filmes.Api.Domain.Interface;

namespace Filmes.Api.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FilmesApiContext _context;

        public UnitOfWork(FilmesApiContext context)
        {
            _context = context;
        }

        public bool Save() => _context.SaveChanges() > 0;
    }
}
