﻿using Filmes.Api.Domain.Entities.Votacao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Filmes.Api.Data.EntityConfig.Votacao
{
    public class VotoMap : IEntityTypeConfiguration<Voto>
    {
        public void Configure(EntityTypeBuilder<Voto> builder)
        {
            //builder.ToTable("filmeAtor");
            builder.HasKey(x => x.Id);
            builder.HasKey(v => new { v.IdFilme , v.IdUsuario });
            builder.HasOne(v => v.Filme)
                .WithMany(f => f.Votos)
                .HasForeignKey(v => v.IdFilme);
            builder.HasOne(v => v.Usuario)
                .WithMany(u => u.Votos)
                .HasForeignKey(v => v.IdUsuario);
        }
    }
}
