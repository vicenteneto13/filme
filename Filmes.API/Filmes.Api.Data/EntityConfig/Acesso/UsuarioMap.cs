﻿using Filmes.Api.Domain.Entities.Acesso;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Filmes.Api.Data.EntityConfig.Midia
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            //builder.ToTable("usuario");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Email)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(40)")
                .IsRequired();
            builder.Property(x => x.Senha)
                .HasColumnType("varchar(20)")
                .IsRequired();
            builder.Property(x => x.Ativo)
                .IsRequired();
            builder.Property(x => x.Administrador)
                .IsRequired();
        }
    }
}
