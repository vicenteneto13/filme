﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Filmes.Api.Data.EntityConfig.Midia
{
    public class FilmeMap : IEntityTypeConfiguration<Domain.Entities.Midia.Filme>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Midia.Filme> builder)
        {
            //builder.ToTable("filme");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Nome)
                .IsRequired();
            builder.Property(x => x.Diretor)
                .IsRequired();
            builder.Property(x => x.Genero)
                .IsRequired();
            builder.Property(x => x.Lancamento)
                .HasColumnType("datetime")
                .IsRequired();
        }
    }
}
