﻿using Filmes.Api.CrossCutting.Authenticated;
using Filmes.Api.Data.Context;
using Filmes.Api.Data.Extensions;
using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Interface.Repositories.Acesso;
using Filmes.Api.Domain.Paged;
using System.Linq;

namespace Filmes.Api.Data.Repositories.Acesso
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(FilmesApiContext context) : base(context)
        {
        }

        public Usuario ObterUsuarioPorEmail(string email)
        {
            return _context.Set<Usuario>()
                .Where(x => x.Email == email)
                .FirstOrDefault();
        }

        public PagedResult<UsuarioDto> ListarUsuariosNaoAdministradoresPaginado(int page, int pageSize)
        {
            return _context.Set<UsuarioDto>()
                    .Where(u => !u.Administrador)
                    .OrderBy(u => u.Nome)
                    .GetPaged(page, pageSize);
        }
    }
}