﻿using Filmes.Api.Data.Context;
using Filmes.Api.Domain.Entities.Votacao;
using Filmes.Api.Domain.Interface.Repositories.Votacao;
using System.Collections.Generic;
using System.Linq;

namespace Filmes.Api.Data.Repositories.Votacao
{
    public class VotoRepository : RepositoryBase<Voto>, IVotoRepository
    {
        public VotoRepository(FilmesApiContext context) : base(context)
        {
        }

        public IList<Voto> ListarPorFilme(int idFilme)
        {
            return _context.Set<Voto>()
                .Where(f => f.IdFilme == idFilme)
                .ToList();
        }

    }
}