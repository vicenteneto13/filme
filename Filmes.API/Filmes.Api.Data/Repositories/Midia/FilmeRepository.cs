﻿using Filmes.Api.Data.Context;
using Filmes.Api.Data.Extensions;
using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Entities.Midia;
using Filmes.Api.Domain.Interface.Repositories.Midia;
using Filmes.Api.Domain.Paged;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Filmes.Api.Data.Repositories.Midia
{
    public class FilmeRepository : RepositoryBase<Filme>, IFilmeRepository
    {
        public FilmeRepository(FilmesApiContext context) : base(context)
        {
        }

        public PagedResult<Filme> ListarPorFiltroAvancadoPaginado(FiltrosFilmeDto filtrosFilmeDto)
        {
            return _context.Set<Filme>()
                .Include(f => f.Votos)
                .Where(f => f.Diretor.ToLower().Contains(filtrosFilmeDto.Descricao) ||
                            f.Nome.ToLower().Contains(filtrosFilmeDto.Descricao) ||
                            f.Genero.ToLower().Contains(filtrosFilmeDto.Descricao))
                .GetPaged(filtrosFilmeDto.PageIndex, filtrosFilmeDto.PageSize);
        }


        public override Filme GetById(int id)
        {
            return _context.Set<Filme>()
                .Include(f => f.Votos)
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

    }
}