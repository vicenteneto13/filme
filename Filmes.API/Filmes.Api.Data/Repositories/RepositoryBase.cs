﻿using Filmes.Api.Data.Context;
using Filmes.Api.Data.Extensions;
using Filmes.Api.Domain.Interface.Repositories;
using Filmes.Api.Domain.Paged;
using System.Collections.Generic;
using System.Linq;

namespace Filmes.Api.Data.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly FilmesApiContext _context;
        
        public RepositoryBase(FilmesApiContext context)
        {
            _context = context;
        }

        public virtual void Add(TEntity obj)
        {
            _context.Set<TEntity>().Update(obj);
        }

        public virtual PagedResult<TEntity> GetAll(int page, int pageSize)
        {
            return _context.Set<TEntity>().GetPaged<TEntity>(page, pageSize);
        }

        public virtual TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public virtual void Remove(TEntity obj)
        {
            _context.Set<TEntity>().Remove(obj);
        }

        public virtual void Update(TEntity obj)
        {
            _context.Set<TEntity>().Update(obj);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }
    }
}