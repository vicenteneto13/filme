﻿using Filmes.Api.AppServices.Services;
using Filmes.Api.AppServices.Services.Acesso;
using Filmes.Api.AppServices.Services.Midia;
using Filmes.Api.Domain.Interface.Services;
using Filmes.Api.Domain.Interface.Services.Acesso;
using Filmes.Api.Domain.Interface.Services.Midia;
using System;
using System.Collections.Generic;

namespace Filmes.Api.AppServices.Ioc
{
    public static class Module
    {
        public static Dictionary<Type,Type> GetTypes()
        {
            var dic = new Dictionary<Type, Type>();
            dic.Add(typeof(IServiceBase<,>), typeof(ServiceBase<,>));
            dic.Add(typeof(IMidiaService), typeof(MidiaService));
            dic.Add(typeof(IUsuarioService), typeof(UsuarioService));

            return dic;
        }
    }
}
