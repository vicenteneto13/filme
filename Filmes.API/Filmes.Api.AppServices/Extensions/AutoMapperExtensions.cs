﻿using System;

namespace Filmes.Api.AppServices.Extensions
{
    public static class AutoMapperExtensions
    {
        public static T MapTo<T>(this object value)
        {
            return AutoMapper.Mapper.Map<T>(value);
        }

        public static object MapTo(this object destination, object source)
        {
            return AutoMapper.Mapper.Map(source, destination, source.GetType(), destination.GetType());
        }

        //public static IEnumerable<T> EnumerableTo<T>(this object value)
        //{
        //    return AutoMapper.Mapper.Map<IEnumerable<T>>(value);
        //}

        //public static PagedResult<T> PagedResultTo<T>(this object value)
        //{
        //    return AutoMapper.Mapper.Map<PagedResult<T>>(value);
        //}
    }
}