﻿using Filmes.Api.AppServices.Extensions;
using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Exceptions;
using Filmes.Api.Domain.Interface;
using Filmes.Api.Domain.Interface.Repositories.Acesso;
using Filmes.Api.Domain.Interface.Services.Acesso;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.AppServices.Services.Acesso
{
    public class UsuarioService : ServiceBase<Usuario, UsuarioDto>, IUsuarioService
    {
        protected readonly IUsuarioRepository _repository;

        public UsuarioService(
            IUsuarioRepository repository,
            IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
            _repository = repository;
        }

        public ValidarUsuarioDto ValidarAcesso(string email, string senha)
        {
            var usuario = _repository.ObterUsuarioPorEmail(email);
            if (usuario == null)
                throw new AuthenticaionException();

            return new ValidarUsuarioDto
            {
                Id = usuario.Id,
                Email = usuario.Email,
                Nome = usuario.Nome,
                Valido = usuario.ValidarCredencial(senha),
                Administrador = usuario.Administrador
            };
        }

        public bool Add(NovoUsuarioDto usuarioDto)
        {
            Execute(() =>
            {
                var usuario = usuarioDto.MapTo<Usuario>();
                _repository.Add(usuario);
            });

            return true;
        }

        public bool Desativar(int id)
        {
            Execute(() =>
            {
                var usuario = _repository.GetById(id);
                usuario.Ativo = false;
                _repository.Update(usuario);
            });

            return true;
        }

        public virtual PagedResult<UsuarioDto> ListarUsuariosNaoAdministradoresPaginado(int page, int pageSize) =>
            _repository.ListarUsuariosNaoAdministradoresPaginado(page, pageSize);

    }
}