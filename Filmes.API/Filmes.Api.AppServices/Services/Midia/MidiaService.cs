﻿using Filmes.Api.AppServices.Extensions;
using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Entities.Midia;
using Filmes.Api.Domain.Entities.Votacao;
using Filmes.Api.Domain.Exceptions;
using Filmes.Api.Domain.Interface;
using Filmes.Api.Domain.Interface.Repositories.Acesso;
using Filmes.Api.Domain.Interface.Repositories.Midia;
using Filmes.Api.Domain.Interface.Repositories.Votacao;
using Filmes.Api.Domain.Interface.Services.Midia;
using Filmes.Api.Domain.Paged;
using System;

namespace Filmes.Api.AppServices.Services.Midia
{
    public class MidiaService : ServiceBase<Domain.Entities.Midia.Filme, FilmeDto>, IMidiaService
    {
        protected readonly IFilmeRepository _repository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IVotoRepository _votoRepository;

        public MidiaService(IFilmeRepository repository,
            IUsuarioRepository usuarioRepository,
            IVotoRepository votoRepository,
            IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
            _repository = repository;
            _usuarioRepository = usuarioRepository;
            _votoRepository = votoRepository;
        }

        public bool Add(NovoFilmeDto filmeDto)
        {
            Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(filmeDto.Nome))
                    throw new RequiredFieldException("Nome é obrigatório");
                if (string.IsNullOrWhiteSpace(filmeDto.Diretor))
                    throw new RequiredFieldException("Diretor é obrigatório");
                if (string.IsNullOrWhiteSpace(filmeDto.Genero))
                    throw new RequiredFieldException("Gênero é obrigatório");
                if (filmeDto.Lancamento == null || DateTime.MinValue == filmeDto.Lancamento)
                    throw new RequiredFieldException("Data de lançamento é obrigatório");

                var filme = filmeDto.MapTo<Filme>();
                _repository.Add(filme);
            });

            return true;
        }

        public override bool Remove(int id)
        {
            Execute(() =>
            {
                var filme = _repository.GetById(id);
                if (filme == null)
                    throw new InvalidDomainException("Id inválido! Filme não encontrado para exclusão.");

                filme.RemoverVotos();
                _repository.Remove(filme);
            });

            return true;
        }

        public override bool Update(FilmeDto filmeDto)
        {
            Execute(() =>
            {
                var filme = _repository.GetById(filmeDto.Id);
                if (filme == null)
                    throw new InvalidDomainException("Id inválido! Filme não encontrado para alteração.");

                if (string.IsNullOrWhiteSpace(filmeDto.Nome))
                    throw new RequiredFieldException("Nome é obrigatório");
                if (string.IsNullOrWhiteSpace(filmeDto.Diretor))
                    throw new RequiredFieldException("Diretor é obrigatório");
                if (string.IsNullOrWhiteSpace(filmeDto.Genero))
                    throw new RequiredFieldException("Gênero é obrigatório");
                if (filmeDto.Lancamento == null || DateTime.MinValue == filmeDto.Lancamento)
                    throw new RequiredFieldException("Data de lançamento é obrigatório");

                filme.MapTo(filmeDto);
                _repository.Update(filme);
            });

            return true;
        }

        public bool Votar(string emailUsuario, VotarFilmeDto votarDto)
        {
            Execute(() =>
            {
                var usuario = _usuarioRepository.ObterUsuarioPorEmail(emailUsuario);
                var filme = _repository.GetById(votarDto.IdFilme);
                var voto = new Voto()
                {
                    IdFilme = filme.Id,
                    Filme = filme,
                    IdUsuario = usuario.Id,
                    Usuario = usuario,
                    ValorVoto = votarDto.ValorVoto
                };

                _votoRepository.Add(voto);
            });

            return true;
        }

        public PagedResult<PesquisaFilmeDto> ListarPorFiltroAvancadoPaginado(FiltrosFilmeDto filtrosFilmeDto)
        {
            filtrosFilmeDto.Descricao ??= string.Empty;
            filtrosFilmeDto.Descricao = filtrosFilmeDto.Descricao.ToLower();

            if (filtrosFilmeDto.PageIndex <= 0 || filtrosFilmeDto.PageSize <= 0)
                throw new InvalidFieldException("Número e / ou tamanho da página invalido(s).");

            var pagina = _repository.ListarPorFiltroAvancadoPaginado(filtrosFilmeDto);
            var paginaDto = pagina.MapTo<PagedResult<PesquisaFilmeDto>>();

            //paginaDto.Results = paginaDto.Results
            //    .OrderByDescending(f => f.QuantidadeVotos)
            //    .ThenBy(f => f.Nome)
            //    .ToList();
            return paginaDto;
        }
    }
}