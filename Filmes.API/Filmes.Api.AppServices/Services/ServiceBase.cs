﻿using System;
using System.Collections.Generic;
using Filmes.Api.AppServices.Extensions;
using Filmes.Api.Domain.Exceptions;
using Filmes.Api.Domain.Interface;
using Filmes.Api.Domain.Interface.Repositories;
using Filmes.Api.Domain.Interface.Services;
using Filmes.Api.Domain.Paged;

namespace Filmes.Api.AppServices.Services
{
    public class ServiceBase<TEntity, TEntityDto> : IServiceBase<TEntity, TEntityDto> where TEntity : class where TEntityDto : class
    {
        protected readonly IRepositoryBase<TEntity> _repository;
        private readonly IUnitOfWork _unitOfWork;

        public ServiceBase(
            IRepositoryBase<TEntity> repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public virtual bool Add(TEntityDto obj)
        {
            Execute(() =>
            {
                var entity = obj.MapTo<TEntity>();
                _repository.Add(entity);
            });

            return true;
        }

        public virtual PagedResult<TEntityDto> GetAll(int page, int pageSize)
        {
            var entitys = _repository.GetAll(page, pageSize);
            if (entitys.PageCount == 0)
                throw new NotFoundRecordException();
            return entitys.MapTo<PagedResult<TEntityDto>>();
        }

        public IEnumerable<TEntityDto> GetAll()
        {
            var entitys = _repository.GetAll();
            return entitys.MapTo<IEnumerable<TEntityDto>>();
        }

        public virtual TEntityDto GetById(int id)
        {
            var entity = _repository.GetById(id);
            if (entity == null)
                throw new NotFoundRecordException();
            return entity.MapTo<TEntityDto>();
        }

        public virtual bool Remove(int id)
        {
            Execute(() =>
            {
                var entity = _repository.GetById(id);
                _repository.Remove(entity);
            });

            return true;
        }

        public virtual bool Update(TEntityDto obj)
        {
            Execute(() =>
            {
                var entity = obj.MapTo<TEntity>();
                _repository.Update(entity);
            });

            return true;
        }

        protected void Execute(Action action)
        {
            try
            {
                action();
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
