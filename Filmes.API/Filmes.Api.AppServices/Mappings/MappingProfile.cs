﻿using Filmes.Api.Domain.Dtos.Acesso;
using Filmes.Api.Domain.Dtos.Midia;
using Filmes.Api.Domain.Entities.Acesso;
using Filmes.Api.Domain.Entities.Midia;

namespace ToDoApp.AppServices.Mappings
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<FilmeDto, Filme>().ReverseMap();
            CreateMap<NovoFilmeDto, Filme>().ReverseMap();
            CreateMap<PesquisaFilmeDto, Filme>().ReverseMap();
            CreateMap<UsuarioDto, Usuario>().ReverseMap();
            CreateMap<NovoUsuarioDto, Usuario>().ReverseMap();
        }
    }
}