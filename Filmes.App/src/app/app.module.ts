import { ControlModule } from './components/controls/control.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { GlobalErrorHandler } from './erros/global-error-handler';
import { HttpErrorInterceptor } from './erros/http-error.interceptor';
import { BaseService } from './services/base.service';
import { NotificationService } from './services/notification.service';
import { MatPaginatorIntlCro } from './services/mat-paginator-intl-cro';
import { MOMENT_DATE_FORMATS, MomentDateAdapter } from './services/moment-date-adapter';
import { AuthGuard } from './services/auth.guard';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { SharedModule } from './shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationDialogComponent } from './components/controls/confirmation-dialog/confirmation-dialog.component';
import { DisableControlDirective } from './components/directive/disableControl/disable-control-directive';
import { BrowserModule } from '@angular/platform-browser';
import { NavigationModule } from './components/navigation/navigation.module';

registerLocaleData(localePt, 'pt-BR');

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/components.module').then(m => m.ComponentsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  }
]

@NgModule({
  declarations: [
    AppComponent,
    DisableControlDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    NavigationModule,
    ControlModule,
    RouterModule.forRoot(routes)
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: MAT_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro },
    { provide: DateAdapter, useClass: MomentDateAdapter },
    BaseService,
    NotificationService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
