import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-date-picker-custom',
  templateUrl: './date-picker-custom.component.html',
  styleUrls: ['./date-picker-custom.component.css']
})
export class DatePickerCustomComponent {
  // dateForm: FormGroup;

  constructor() { // <--- inject FormBuilder
    //this.createForm();
  }

  ngOnInit() { }

  createForm() {
    // this.dateForm = this.fb.group({
    //   datePickerInput: ['', Validators.compose([
    //     Validators.required,
    //     //this.validateDate
    //   ])]
    // });
  }

  validateDate(controls) {
    const regExp = new RegExp(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/);
    //  if (regExp.test(controls.value)) {
    //    return null;
    //  } else {
    //    return { 'validateDate': true }
    //  }
    return regExp.test(controls.value) ? null : { 'validateDate': true };
  }

  // Prevent no number type input, valid characters in input are numbers only
  _keyPress(event: any) {
    const pattern = /^[0-9]*$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
}