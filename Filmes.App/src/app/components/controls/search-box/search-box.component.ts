import { Component, EventEmitter, ElementRef, ViewChild, Output, Input } from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent {

  @Input()
  placeholder = 'Search...';

  @Output()
  searchChange = new EventEmitter<string>();

  @ViewChild('searchInput', { static: true })
  searchInput: ElementRef;

  onValueChange(value: string) {
      this.searchChange.emit(value);
  }

  clear() {
      this.searchInput.nativeElement.value = '';
      this.onValueChange(this.searchInput.nativeElement.value);
  }

}
