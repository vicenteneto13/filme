import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { DatePickerCustomComponent } from './date-picker-custom/date-picker-custom.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
    declarations: [
        LoaderComponent,
        SearchBoxComponent,
        DatePickerCustomComponent,
        ConfirmationDialogComponent,
    ],
    imports: [
        SharedModule
    ],
    exports: [
        LoaderComponent,
        SearchBoxComponent,
        DatePickerCustomComponent,
        ConfirmationDialogComponent,
    ],
    providers: [],
})
export class ControlModule { }
