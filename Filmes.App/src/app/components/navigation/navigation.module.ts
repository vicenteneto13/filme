import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { ButtonBackComponent } from './button-back/button-back.component';
import { ButtonAddComponent } from './button-add/button-add.component';
import { ButtonEditComponent } from './button-edit/button-edit.component';
import { ButtonDeleteComponent } from './button-delete/button-delete.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { PagedComponent } from './paged/paged.component';


@NgModule({
    declarations: [
        ButtonBackComponent,
        ButtonAddComponent,
        ButtonEditComponent,
        ButtonDeleteComponent,
        HeaderComponent,
        HomeComponent,
        FooterComponent,
        PagedComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        ButtonBackComponent,
        ButtonAddComponent,
        ButtonEditComponent,
        ButtonDeleteComponent,
        HeaderComponent,
        HomeComponent,
        FooterComponent,
        PagedComponent
    ],
    providers: [],
})
export class NavigationModule { }
