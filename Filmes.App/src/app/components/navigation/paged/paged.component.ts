import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PagedResult } from 'src/app/models/resultado/PagedResult';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-paged',
  templateUrl: './paged.component.html',
  styleUrls: ['./paged.component.css']
})
export class PagedComponent {

  pagedOptions: number[] = [10, 20, 30];
  pageIndex: number;
  pageSize: number;
  length: number;

  beginEvent = {
    length: 0,
    pageIndex: 0,
    pageSize: this.pagedOptions[0]
  };

  @Output()
  eventPaged = new EventEmitter<PageEvent>();

  public getServerData(event?: PageEvent) {
    if (event === undefined) {
      event = this.beginEvent;
    }
    this.eventPaged.emit(event);
    // setTimeout(() => this.eventPaged.emit(event));
  }

  public setPage(page: PagedResult<any>){
    this.length = page.RowCount
    this.pageSize = page.PageSize
    this.pageIndex = page.CurrentPage - 1
  }

}
