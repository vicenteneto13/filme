import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>

  constructor(private authService: AuthService,
    private _router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn
  }

  onLogout() {
    this.authService.logout()
  }

  filmes() {
    this._router.navigate(['filme', 'lista'])
  }

  home() {
    this._router.navigate(['/'])
  }

}
