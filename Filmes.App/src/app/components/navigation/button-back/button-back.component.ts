import { RouterExtService } from './../../../services/router-ext.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-button-back',
  templateUrl: './button-back.component.html',
  styleUrls: ['./button-back.component.css']
})
export class ButtonBackComponent implements OnInit {

  @Input() titulo: string;
  
  constructor(private _routerExt: RouterExtService,
              private _router: Router) { }

  ngOnInit() {
  }

  voltarUrl() {
    this._routerExt.getPreviousUrl();
    // let urlAnterior = this._routerExt.getPreviousUrl()
    // if (urlAnterior !== '')
    //   this._router.navigate([`/${urlAnterior}`])
  }

}
