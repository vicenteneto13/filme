import { DetalheFilmeComponent } from './detalhe/detalhe.component';
import { ListaFilmeComponent } from './lista/lista.component';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/services/auth.guard';
import { FilmeService } from 'src/app/services/filme.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { NavigationModule } from '../navigation/navigation.module';
import { ControlModule } from '../controls/control.module';

const routes: Routes = [
    {
        path: 'lista',
        component: ListaFilmeComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'detalhe',
        component: DetalheFilmeComponent,
        canActivateChild: [AuthGuard]
    },
    {
        path: 'detalhe/:id',
        component: DetalheFilmeComponent,
        canActivateChild: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'lista'
    }
]

@NgModule({
    declarations: [
        ListaFilmeComponent,
        DetalheFilmeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        NavigationModule,
        ControlModule
    ],
    providers: [
        FilmeService
    ],
})
export class FilmeModule { }
