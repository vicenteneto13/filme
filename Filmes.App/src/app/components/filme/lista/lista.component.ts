import { GenericSimpleResult } from '../../../models/resultado/GenericSimpleResult';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FilmeService } from 'src/app/services/filme.service';

import { GenericResult } from 'src/app/models/resultado/GenericResult';
import { PagedResult } from 'src/app/models/resultado/PagedResult';
import { PagedComponent } from '../../navigation/paged/paged.component';
import { NotificationService } from 'src/app/services/notification.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { FiltroFilme } from 'src/app/models/filtro/FiltroFilme';

@Component({
  selector: 'lista-filme',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaFilmeComponent implements OnInit {
  displayedColumns: string[] = ['nome', 'genero', 'lancamento', 'editar', 'excluir'];
  dataSource = new MatTableDataSource<any>();
  filmes: any[] = [];
  filtro: FiltroFilme = new FiltroFilme();

  @ViewChild(MatSort)
  private sort: MatSort;

  @ViewChild('paged', { static: true })
  private paginate: PagedComponent;

  constructor(
    private _service: FilmeService,
    private _notification: NotificationService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.carregarFilmes(this.paginate.beginEvent)
  };

  carregarFilmes(event: PageEvent): PageEvent {
    this.filtro.pageIndex = event.pageIndex;
    this.filtro.pageSize = event.pageSize;
    this._service.getPagedWithFilter<GenericResult<PagedResult<any>>>(this.filtro)
      .subscribe((data) => {
        this.configurarPagina(data.Result)
      })

    return event
  }

  private configurarPagina(filmes: any) {
    this.filmes = filmes.Results

    this.paginate.length = filmes.rowCount
    this.paginate.pageSize = filmes.pageSize
    this.paginate.pageIndex = filmes.currentPage - 1

    this.dataSource.data = this.filmes
    this.dataSource.sort = this.sort
  }

  remover(id: number) {
    if (id > 0) {
      this._notification.showConfirm("Deseja realmente excluir o filme?")
        .subscribe(result => {
          if (result) {
            this._service.delete<GenericSimpleResult>(id)
              .subscribe(data => {
                if (data.Success) {
                  this._notification.showSuccess('Filme excluído')
                  this.carregarFilmes(this.paginate.beginEvent)
                }
              })
          }
        })
    }
  }

  alterar(id: number) {
    this._router.navigate(['filme', 'detalhe', id])
  }

  onSearchChanged(value: string) {
    this.filtro.descricao = value;
    this._service.getPagedWithFilter<GenericResult<PagedResult<any>>>(this.filtro)
      .subscribe(data => {
        if (data.Success) {
          this.configurarPagina(data.Result)
        }
      })
  }

}