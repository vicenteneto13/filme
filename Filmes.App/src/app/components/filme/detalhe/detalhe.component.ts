import { DatePipe } from '@angular/common';
import { FilmeService } from '../../../services/filme.service';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/services/notification.service';
import { GenericSimpleResult } from 'src/app/models/resultado/GenericSimpleResult';
import { Router, ActivatedRoute } from '@angular/router';
import { GenericResult } from 'src/app/models/resultado/GenericResult';

@Component({
  selector: 'detalhe-filme',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.css']
})
export class DetalheFilmeComponent implements OnInit {

  form: FormGroup
  filme: any
  editando: boolean = false
  private pipe: DatePipe = new DatePipe('pt-BR')

  constructor(private _formBuilder: FormBuilder,
    private _filmeService: FilmeService,
    private _notification: NotificationService,
    private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit() {
    this._editar()
  }

  private _editar() {
    let idFilme = this._route.snapshot.params['id']
    if (idFilme > 0) {
      this.editando = true
      this._filmeService.get<GenericResult<any>>(idFilme)
        .subscribe(data => {
          if (data.Success) {
            this.filme = data.Result
            this._criarFormulario()
          }
        })
    } else {
      this.filme = {
        Id: null,
        Nome: null,
        Genero: null,
        Diretor: null,
        Lancamento: null,
      }
      this._criarFormulario()
    }
  }

  private _criarFormulario() {
    this.form = this._formBuilder.group({
      Id: [this.filme.Id],
      Nome: [this.filme.Nome, Validators.required],
      Genero: [this.filme.Genero, Validators.required],
      Diretor: [this.filme.Diretor, Validators.required],
      Lancamento: [this.filme.Lancamento, Validators.required],
    })
  }

  salvar() {
    let filme = this.filme

    if (filme?.Id > 0)
      this._alterarFilme(filme)
    else
      this._incluirFilme(filme)
  }

  private _incluirFilme(filme: any) {
    this._filmeService.post<GenericSimpleResult>(filme)
      .subscribe(data => {
        if (data.Success) {
          this._notification.showSuccess('filme salvo')
          this._router.navigate(['filme/lista'])
        }
      })
  }

  private _alterarFilme(filme: any) {
    this._filmeService.put<GenericSimpleResult>(filme.Id, filme)
      .subscribe(data => {
        if (data.Success) {
          this._notification.showSuccess('Filme alterado')
          this._router.navigate(['filme/lista'])
        }
      })
  }

  get criacao() {
    return this.pipe.transform(this.form.get('pessoa').get('criacao').value, 'dd/MM/yyyy HH:mm:ss')
  }

}
