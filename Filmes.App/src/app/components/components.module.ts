import { AuthGuard } from 'src/app/services/auth.guard';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'filme',
    loadChildren: () => import('./filme/filme.module').then(m => m.FilmeModule),
    canActivate: [AuthGuard]
  }
]

@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [],
})
export class ComponentsModule { }
