import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends BaseService {

  constructor(private _http: HttpClient) {
    super(_http)
    this.path = 'Usuarios/'
  }
}
