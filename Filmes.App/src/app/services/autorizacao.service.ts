import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutorizacaoService extends BaseService {

  constructor(private _http: HttpClient) {
    super(_http)
    this.path = 'Autorizacoes/'
  }

  public login<T>(usuario: string, senha: string): Observable<T> {
    if (usuario === '' || senha === '')
      return

    return this.http.post<T>(`${this.url}${this.path}${usuario}/${senha}`, '')
  }
}
