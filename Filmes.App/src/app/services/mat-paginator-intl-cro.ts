import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';

@Injectable({
  providedIn: 'root'
})
export class MatPaginatorIntlCro extends MatPaginatorIntl {
  itemsPerPageLabel = 'Itens por página'
  nextPageLabel     = 'Próxima página'
  previousPageLabel = 'Página anterior'
  firstPageLabel = 'Primeira página'
  lastPageLabel = 'Última página'
}