import { Injectable, Inject, Injector } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { ConfirmationDialogComponent } from '../components/controls/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly titleErro: string = "Erro"
  private readonly titleSucesso: string = "Sucesso"
  private readonly titleInfo: string = "Aviso"
  private readonly timeOut: number = 3000
  private readonly progressBar: boolean = true

  constructor(@Inject(Injector) private _injector: Injector,
    private _dialog: MatDialog) { }

  // Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
  private get toastrService(): ToastrService {
    return this._injector.get(ToastrService)
  }

  public showSuccess(message: string) {
    if (message !== '')
      this.toastrService.success(message, this.titleSucesso, { timeOut: this.timeOut, progressBar: this.progressBar })
  }

  public showError(message: string) {
    if (message !== '')
      this.toastrService.error(message, this.titleErro, { timeOut: this.timeOut, progressBar: this.progressBar });
  }

  public showInfo(message: string) {
    if (message !== '')
      this.toastrService.info(message, this.titleInfo, { timeOut: this.timeOut, progressBar: this.progressBar });
  }

  public showConfirm(message: string) {
    if (message !== '')
      return this._dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: message,
        position: { top: '10px' }
      }).afterClosed()

    // return this.toastrService.warning("<br/><button type='button' value='yes'>Yes</button>&nbsp&nbsp&nbsp&nbsp<button type='button' value='no'>No</button>", message,
    //   {
    //     closeButton: false,
    //     enableHtml: true,
    //     positionClass: "toast-top-center",
    //     on
    //   })
  }
}
