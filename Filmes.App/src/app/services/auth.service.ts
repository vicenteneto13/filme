import { KeyStorage } from './../util/keyStorage';
import { Token } from '../models/comum/Token';
import { AutorizacaoService } from './autorizacao.service';
import { BehaviorSubject } from 'rxjs';
import { Usuario } from '../models/comum/Usuario';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private _router: Router,
              private _autorizacao: AutorizacaoService) { }

  get isLoggedIn() {
    if (this.isAuthenticated())
      this.loggedIn.next(true)

    return this.loggedIn.asObservable();
  }

  login(user: Usuario) {
    if (user.nome !== '' && user.senha !== '') {
      this._autorizacao.login<Token>(user.nome, user.senha).subscribe(data => {
        if (data.authenticated && data.accessToken !== '') {
          sessionStorage.setItem(KeyStorage.tokenUsuario, data.accessToken)
          this.loggedIn.next(true)
          this._router.navigate(['/'])
        }
      })
    }
  }

  logout() {
    sessionStorage.removeItem(KeyStorage.tokenUsuario)
    this.loggedIn.next(false)
    this._router.navigate(['/login'])
  }

  isAuthenticated() {
    const token = sessionStorage.getItem(KeyStorage.tokenUsuario)
    return token !== null
  }
}
