import { FiltroFilme } from './../models/filtro/FiltroFilme';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class FilmeService extends BaseService {

  constructor(private _http: HttpClient) {
    super(_http);
    this.path = 'Filmes/'
  }

  public getPagedWithFilter<T>(filtro: FiltroFilme): Observable<T> {
    filtro.pageIndex = filtro.pageIndex === 0 ? 1 : filtro.pageIndex
    filtro.pageSize = filtro.pageSize ?? this.sizePageDefault
    
    let params = new HttpParams();
    params = params.append('PageIndex', filtro.pageIndex.toString());
    params = params.append('PageSize', filtro.pageSize.toString());
    params = params.append('Descricao', filtro.descricao);
    return this.http.get<T>(this.url + this.path, { headers: this.headers, params: params })
    // return this.http.get<T>(this.url + this.path, { headers: this.headers, params: this.getParams(filtro) })
  }
}
