import { KeyStorage } from './../util/keyStorage';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected url: string = environment.apiUrl
  protected path: string = ""
  protected sizePageDefault = 5

  constructor(protected http: HttpClient) { }

  public getPaged<T>(pageIndex: number, pageSize: number): Observable<T> {
    pageIndex = pageIndex + 1
    pageSize = pageSize === undefined ? this.sizePageDefault : pageSize

    return this.http.get<T>(this.url + this.path + pageIndex + "/" + pageSize, { headers: this.headers })
  }

  public getPagedSearch<T>(pageIndex: number, pageSize: number, value: any): Observable<T> {
    pageIndex = pageIndex + 1
    pageSize = pageSize === undefined ? this.sizePageDefault : pageSize

    return this.http.get<T>(this.url + this.path + pageIndex + "/" + pageSize + "/" + value, { headers: this.headers })
  }

  public getAll<T>(): Observable<T> {
    return this.http.get<T>(this.url + this.path, { headers: this.headers })
  }

  public get<T>(value: any): Observable<T> {
    if (value === null)
      return

    return this.http.get<T>(this.url + this.path + value, { headers: this.headers })
  }

  public post<T>(value: any): Observable<T> {
    if (value === null)
      return

    return this.http.post<T>(this.url + this.path, value, { headers: this.headers })
  }

  public put<T>(key: number, value: any): Observable<T> {
    if (value === null)
      return

    return this.http.put<T>(this.url + this.path + key, value, { headers: this.headers })
  }

  public delete<T>(key: number): Observable<T> {
    if (key <= 0)
      return

    return this.http.delete<T>(this.url + this.path + key, { headers: this.headers })
  }

  protected get headers() {
    let token = sessionStorage.getItem(KeyStorage.tokenUsuario)

    return new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    })
  }

  protected getParams(params: any) {
    let httpParams = new HttpParams();

    for (var p in params) {
      if (p !== 'constructor')
        httpParams.append(p, params[p].toString());
    }

    return httpParams;
  }

}