import { Router, NavigationEnd } from '@angular/router';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class RouterExtService {
  private previousUrl: string
  private currentUrl: string

  constructor(private _router: Router,
              private _location: Location) {
    this.currentUrl = this._router.url
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl
        this.currentUrl = event.url
      }
    })
  }

  getPreviousUrl() {
    // return this.previousUrl
    this._location.back();
  }

  getcurrentUrl() {
    return this.currentUrl
  }

}
