import { LoaderService } from './../services/loader.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    private requests: HttpRequest<any>[] = []

    constructor(private _loaderService: LoaderService) { }

    removeRequest(req: HttpRequest<any>) {
        const i = this.requests.indexOf(req)
        if (i >= 0) {
            this.requests.splice(i, 1)
        }
        this._loaderService.isLoading.next(this.requests.length > 0)
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.requests.push(request)

        this._loaderService.isLoading.next(true)

        return Observable.create(observer => {
            const subscription = next.handle(request)
                .subscribe(
                    event => {
                        if (event instanceof HttpResponse) {
                            this.removeRequest(request)
                            observer.next(event)
                        }
                    },
                    (error: HttpErrorResponse) => {
                        this.removeRequest(request)
                        observer.error(error)
                    },
                    () => {
                        this.removeRequest(request)
                        observer.complete()
                    });
            // remove request from queue when cancelled
            return () => {
                this.removeRequest(request)
                subscription.unsubscribe()
            }
        })

        // return next.handle(request)
        // .pipe(
        //     retry(0), // repetir tentativas
        //     catchError((error: HttpErrorResponse) => {
        //         // return Observable.throw(error);
        //         return throwError(error);
        //     })
        // )
    }
}