import { AuthService } from 'src/app/services/auth.service';
import { ErrorHandler, Injectable, Injector } from "@angular/core"
import { UNAUTHORIZED, BAD_REQUEST, FORBIDDEN, NOT_FOUND, INTERNAL_SERVER_ERROR } from "http-status-codes"
import { Router } from "@angular/router"
import { NotificationService } from '../services/notification.service'

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  static readonly REFRESH_PAGE_ON_TOAST_CLICK_MESSAGE: string = "An error occurred: Please click this message to refresh"
  static readonly DEFAULT_ERROR_TITLE: string = "Something went wrong"
  private router: Router

  constructor(private _notification: NotificationService,
    private _injector: Injector,
    private _authService: AuthService) { }

  public handleError(error: any) {
    console.log(error)
    this.router = this._injector.get(Router)

    if (error.error instanceof ErrorEvent) {
      this._notification.showError(`Erro: ${error.error.message}`)
    } else {
      let httpErrorCode = error.status
      let mensagem = this.mensagem(error)
      let mensagemApi = this.mensagemApi(error)

      switch (httpErrorCode) {
        case UNAUTHORIZED:
          if (mensagemApi !== '')
            this._notification.showInfo(mensagemApi)
          else
            this._notification.showInfo('Usuário não autorizado')
          this._authService.logout()
          break
        case FORBIDDEN:
          if (mensagemApi !== '')
            this._notification.showInfo(mensagemApi)
          else
            this._notification.showInfo('Usuário com acesso proibido')
          // this._authService.logout()
          break
        case BAD_REQUEST:
          this._notification.showInfo(mensagem)
          break
        case NOT_FOUND:
          this._notification.showInfo(mensagem)
          break
        case INTERNAL_SERVER_ERROR:
          this._notification.showError(mensagem)
          break
        default:
          this._notification.showError(mensagem)
      }
    }
  }

  private mensagem(error: any) {
    let msgApi = this.mensagemApi(error)
    if (msgApi !== '')
      return msgApi

    if (error.error && error.error.Message !== '')
      return error.message

    if (error.message !== '')
      return error.message

    return ''
  }

  private mensagemApi(error: any) {
    if (error.error && error.error.Error && error.error.Error.Message !== "")
      return error.error.Error.Message
    else
      return ''
  }

}