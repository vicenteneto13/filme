export class Paged {
    length: number
    pageSize: number
    pageIndex: number
}