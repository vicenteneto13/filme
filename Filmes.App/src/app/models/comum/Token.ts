export class Token {
    authenticated: boolean = false
    created: Date = null
    expiration: Date = null
    accessToken: string = ''
    message = ''
}