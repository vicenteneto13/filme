
export class GenericResult<TResult> {
    Errors:  string[];
	Success: boolean;
    Result : TResult
}