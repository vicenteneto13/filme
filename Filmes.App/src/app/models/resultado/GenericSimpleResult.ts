export class GenericSimpleResult {
	Errors:  string[];
	Success: boolean;
}